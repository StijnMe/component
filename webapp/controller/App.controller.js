sap.ui.define(
	[
		"templatepath/controller/BaseController"
	],
	function(BaseController) {
		"use strict";

		/**
		 * @name	ui5expresstemplate.controller.App
		 * @alias 	ui5expresstemplate.controller.App
		 * @constructor
		 * @public
		 * @extends sap.ui.core.mvc.Controller
		 * @class
		 * The basecontroller is inherited by all the controllers of the application. It contains shared functionality that can be triggered
		 * from multiple locations in the app.<br/>
		 **/
		const AppController = BaseController.extend(
			"ui5expresstemplate.controller.App",
			/** @lends ui5expresstemplate.controller.AppController.prototype */
			{
				constructor: function() {}
			}
		);

		/**
		 * initializing event handler
		 * @method onInit
		 * @public
		 * @instance
		 * @memberof ui5expresstemplate.controller.AppController
		 * @public
		 */
		AppController.prototype.onInit = function() {};

		return AppController;
	}
);
