sap.ui.define(
	[
		"sap/ui/core/UIComponent", 
		"templatepath/model/models"
	],
	function(UIComponent, models) {
		"use strict";

		/**
		 * @name	ui5expresstemplate.Component
		 * @alias 	ui5expresstemplate.Component
		 * @constructor
		 * @public
		 * @extends sap.ui.core.mvc.Controller
		 * @class
		 * The main component of your app. This also serves as public interface when your component is embedded in another app.<br/>
		 * Be sure to define properties and events that need to be accessible from outside, as well as public methods.<br/>
		 **/
		const Component = UIComponent.extend(
			"ui5expresstemplate.Component",
			/**@lends ui5expresstemplate.Component.prototype **/ {
				metadata: {
					manifest: "json",
					properties:{
						/**
						 * What is the git URL of your project? Set this so that your the issuelogger plugin can direct you to the right 
						 * location for issues.
						 * @property {string} gitUrl
						 * @memberof ui5expresstemplate.Component
						 */
						"gitUrl":{
						    type:"string",
						    defaultValue:"https://gitlab.com/fiddlebe/templates/component",
						    //mandatory:true,   //this attribute doesn't really exist, but I would like it to.
						    //readonly:true,    //this attribute doesn't really exist, but I would like it to.
						    byValue:"true"
						}
					},
					events:{
						/**
						 * Internal event. When a property on the component is changed (typically from the outside) , we
						 * notify any internal listeners (controllers) that a property was changed. This event typicall
						 * triggers a filtering of the internal dataset.
						 * 
						 * @event _propertyChanged
						 * @memberof ui5expresstemplate.Component
						 * @param {string} property - the name of the property that changed
						 * @param {any} value - the new value of the property
						 */
						"_propertyChanged":{ 
							parameters:{
								"property":"string",
								"value":"any"
							}
						}
					}
				}
			}
		);

		/**
		 * @method	init
		 * @constructor
		 * @public
		 * @memberof	ui5expresstemplate.Component
		 * initialization of manifest, device model (if exists) and router (if exists).<br/>
		 **/
		Component.prototype.init = function() {
			UIComponent.prototype.init.apply(this, arguments);

			// Set the device model
			if (models) {
				this.setModel(models.createDeviceModel(), "device");
			}

			// Initialize the router
			if (this.getRouter() ) {
				this.getRouter().initialize();
			}

			//use the global messagemodel, so you can easily access any messages from odata models
			this.setModel(sap.ui.getCore().getMessageManager().getMessageModel(),"messages"); 
		};

		/**
		 * @method	setProperty
		 * @public
		 * @memberof	ui5expresstemplate.Component
		 * @param {string} name - the name of the property you are setting
		 * @param {any} value - the value for the property you are setting
		 * <p>overload of standard property setter, so we can check if we have the necessary parameters to collect the history log.</p>
		 * <p>Ideally, a component would have a "$this" model, like the composite controls. There is something like the managed object 
		 * model, but if it isn't used automatically by the component, than it has no use. So instead, we do it ourselves here.</p>
		 * <p>Pending better solution</p>
		 * 
		 **/
		Component.prototype.setProperty = function(name, value){
			UIComponent.prototype.setProperty.apply(this, arguments );
			this.getModel("settings").setProperty("/" + name, value );
			this.fireEvent( "_propertyChanged", {property:name, value: value });
		};

		return Component;
	}
);
